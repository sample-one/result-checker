

function checkResult() {
  let marks = parseFloat(document.getElementById('marksInput').value);

  if (!isNaN(marks)) {
      if (marks >= 50) {
          document.getElementById('result').innerText = 'Passed';
      } else {
          document.getElementById('result').innerText = 'Failed';
      }
  } else {
      alert('Please enter a valid number for marks.');
  }
}